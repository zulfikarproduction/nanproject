package id.apps.nan.util;

public class Cfg {
    public static final String BASE_URL = "http://202.150.133.147/api/";
    public static final String BASE_URL_IDENTITY = BASE_URL+"nanidentity/";

    public static final String username = "username";
    public static final String password = "password";
    public static final String idPendamping = "idpendamping";
    public static final String nik = "nik";
    public static final String namaPendamping = "namaPendamping";
    public static final String jenisKelamin = "jenisKelamin";
    public static final String phone = "phone";
    public static final String alamat = "alamat";
    public static final String kelurahan = "kelurahan";
    public static final String kecamatan = "kecamatan";
    public static final String kotaKabupaten = "kotaKabupaten";
    public static final String provinsi = "provinsi";

}
