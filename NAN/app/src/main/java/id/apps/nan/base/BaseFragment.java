package id.apps.nan.base;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.fragment.app.Fragment;

import id.apps.nan.R;
import id.apps.nan.app.BaseApplication;
import id.apps.nan.dialog.DfLoading;


/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {

    Context mContext;

//    public DBTransaction dbTransaction;
    public String recentTransactionId;

    public BaseFragment() {
        // Required empty public constructor
    }

    /*public SharedPreferences getSecPref(){
        return ((BaseApplication)getContext()).secPrefs;
    }*/

    public DfLoading dfLoading;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        dbTransaction=new DBTransaction(getActivity());
//        recentTransactionId=getFragSecPrefs().getString(Cfg.prefsRecentTransIdStr,"");
        return inflater.inflate(R.layout.fragment_base, container, false);
    }

    public String LOG=getClass().getSimpleName();

    public SharedPreferences getFragSecPrefs(){
            return ((BaseApplication) getActivity().getApplication()).secPrefs;
    }

//    public DBTransaction getDbTransaction() {
//        return dbTransaction;
//    }

    public void hideKeyboard(Boolean Hide){
        if (Hide){
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(
                    Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
        else{
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(
                    Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    @Override
    public void onAttach(Context context) {
        //hideKeyboard();
        super.onAttach(context);
        mContext=context;
        dfLoading =new DfLoading();
    }

    public void onBackPressed(){
        onBackPressed();
    }
}
