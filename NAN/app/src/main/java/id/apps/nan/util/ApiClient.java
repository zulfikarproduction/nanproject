package id.apps.nan.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static Retrofit serviceGenerator(String baseUrl){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
//        Log.d("wakacawserviceGenerator","baseUrl : "+baseUrl);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient
                .Builder()
                .retryOnConnectionFailure(true)
//                .connectionPool(new ConnectionPool(5,5,TimeUnit.MINUTES))
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public Response intercept(Chain chain) throws IOException {
//                        Request request = chain.request();
//
//
//                        Headers.Builder headers = new Headers.Builder();
////                        headers.add("Authorization", "Bearer "+Cfg.StringToken);
//                        headers.add("Content-Type", "application/json");
//                        return null;
//                    }
//                })
                .addInterceptor(interceptor)
                .build();
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }



}
