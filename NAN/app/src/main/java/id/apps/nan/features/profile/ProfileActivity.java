package id.apps.nan.features.profile;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import id.apps.nan.R;
import id.apps.nan.base.BaseActivity;
import id.apps.nan.util.Cfg;

public class ProfileActivity extends BaseActivity {

    TextView tvTitle,tvId, tvNama;
    EditText etNik, etTempatLahir, etTanggalLahir, etAlamat, etKelurahan, etKecamatan, etKotaKabupaten, etProvinsi, etNoHp, etKelamin;
    Spinner spinnerKelamin;
    Button btnSubmit;
    ImageView ivProfil, btnBack;
    String jenisProfil;
    ProfilePresenter profilePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initView();
        jenisProfil = getIntent().getExtras().getString("profil");
        if(jenisProfil.equals("pendamping")){
            populateDataPendamping();
            return;
        }
        if(jenisProfil.equals("ppkh")){
            profilePresenter.populateDataPpkh();
            return;
        }
        if(jenisProfil.equals("cpkh")){
//            profilePresenter.newData();
        }
    }

    private void initView(){
        profilePresenter = new ProfilePresenter(this);

        tvId = findViewById(R.id.tv_id);
        tvNama = findViewById(R.id.tv_nama);
        etNik = findViewById(R.id.et_nik);
        etAlamat = findViewById(R.id.et_alamat);
        etKelurahan= findViewById(R.id.et_kelurahan);
        etKecamatan= findViewById(R.id.et_kecamatan);
        etKotaKabupaten= findViewById(R.id.et_kotakabupaten);
        etProvinsi= findViewById(R.id.et_provinsi);
        etNoHp= findViewById(R.id.et_nomor_handphone);
        spinnerKelamin = findViewById(R.id.spinner_kelamin);
        etTempatLahir = findViewById(R.id.et_tempat_lahir);
        etTanggalLahir = findViewById(R.id.et_tanggal_lahir);
        etKelamin = findViewById(R.id.et_kelamin);
        btnSubmit = findViewById(R.id.btn_submit);
        btnBack = findViewById(R.id.btn_back);
    }

    private void populateDataPendamping(){
        final String idPendamping = getSecPref().getString(Cfg.username,"");
        profilePresenter.getProfilePendamping(idPendamping);
    }



}
