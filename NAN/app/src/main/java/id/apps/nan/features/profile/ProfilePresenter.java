package id.apps.nan.features.profile;

import android.util.Log;
import android.view.View;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Map;

import butterknife.internal.ListenerClass;
import id.apps.nan.util.ApiClient;
import id.apps.nan.util.Cfg;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class ProfilePresenter {
    private ProfileActivity profileActivity;
    private String nama="",phone="",alamat="",kelurahan="",kecamatan="", provinsi="",kotaKabupaten="", nik="", kelamin="", rtRw="", tempatLahir="", tanggalLahir="", idPkh;

    public ProfilePresenter(ProfileActivity profileActivity) {
        this.profileActivity = profileActivity;
    }

    public void getProfilePendamping(String id ){
        SvProfile svProfile = ApiClient.serviceGenerator(Cfg.BASE_URL_IDENTITY).create(SvProfile.class);
        Call<Map<String,Object>> call = svProfile.getProfilPendamping(id);
        Log.d("wakacaw", call.toString());
        call.enqueue(new Callback<Map<String,Object>>() {
            @Override
            public void onResponse(Call<Map<String,Object>> call, Response<Map<String,Object>> response) {
                if (!response.isSuccessful()){
                    Toast.makeText(profileActivity, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    profileActivity.onBackPressed();
                    return;
                }
                Map<String,Object> res = response.body();
                if (!res.get("statusCode").equals("00")){
                    Toast.makeText(profileActivity, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    profileActivity.onBackPressed();
                    return;
                }

                Gson gson = new Gson();
                String json =gson.toJson(response.body().get("data"));
                try {
                    JSONObject obj = new JSONObject(json);
                    Log.d("wakacaw", "bisa " + obj.getString("jenisKelamin"));
                    nik = obj.getString("nik");
                    nama = obj.getString("namaPendamping");
                    kelamin = obj.getString("jenisKelamin");
                    phone = obj.getString("phone");
                    alamat = obj.getString("alamat");
                    kelurahan = obj.getString("kelurahan");
                    kecamatan = obj.getString("kecamatan");
                    kotaKabupaten = obj.getString("kotaKabupaten");
                    provinsi = obj.getString("provinsi");

                    Log.d("wakacaw", "bisa " + nik);
                    Log.d("wakacawprofile", "bisa " + "TITITTTTTTT");

                    Log.d(profileActivity.LOG,nama+kotaKabupaten+kecamatan+kelurahan+provinsi+alamat+phone);
                    profileActivity.tvId.setText(id);
                    profileActivity.tvNama.setText(nama);
                    profileActivity.etNik.setText(nik);
                    profileActivity.etKotaKabupaten.setText(kotaKabupaten);
                    profileActivity.etKecamatan.setText(kecamatan);
                    profileActivity.etKelurahan.setText(kelurahan);
                    profileActivity.etProvinsi.setText(provinsi);
                    profileActivity.etAlamat.setText(alamat);
                    profileActivity.etNoHp.setText(phone);
                    profileActivity.etKelamin.setText(kelamin);
//                    profileActivity.spinnerKelamin.setAdapter(adapterSpinner);
                    disableEditting();

                } catch (Throwable t) {
                    Log.e("wakacaw", "Could not parse malformed JSON: \"" + json + "\"");
                }
            }

            @Override
            public void onFailure(Call<Map<String,Object>> call, Throwable t) {

            }
        });
    }


    private void disableEditting(){
        profileActivity.etTanggalLahir.setVisibility(View.GONE);
        profileActivity.etTempatLahir.setVisibility(View.GONE);
        profileActivity.btnSubmit.setVisibility(View.GONE);
        profileActivity.spinnerKelamin.setVisibility(View.GONE);
        profileActivity.etNik.setKeyListener(null);
        profileActivity.etKotaKabupaten.setKeyListener(null);
        profileActivity.etKecamatan.setKeyListener(null);
        profileActivity.etKelurahan.setKeyListener(null);
        profileActivity.etProvinsi.setKeyListener(null);
        profileActivity.etAlamat.setKeyListener(null);
        profileActivity.etNoHp.setKeyListener(null);
    }

    public void populateDataPpkh(){
//        intent.putExtra("profil","ppkh");
//        intent.putExtra("nik",resmm.getData().get(position).getNik());
//        intent.putExtra("tempatLahir",resmm.getData().get(position).getTempatLahir());
//        intent.putExtra("tanggalLahir",resmm.getData().get(position).getTanggalLahir());
//        intent.putExtra("jenisKelamin",resmm.getData().get(position).getJenisKelamin());
//        intent.putExtra("alamat",resmm.getData().get(position).getAlamat());
//        intent.putExtra("kelurahan",resmm.getData().get(position).getKelurahan());
//        intent.putExtra("kecamatan",resmm.getData().get(position).getKecamatan());
//        intent.putExtra("rtRw",resmm.getData().get(position).getRtRw());
//        intent.putExtra("kotaKabupaten",resmm.getData().get(position).getKotaKabupaten());
//        intent.putExtra("provinsi",resmm.getData().get(position).getProvinsi());
//        intent.putExtra("phone",resmm.getData().get(position).getPhone());

        nik = profileActivity.getIntent().getStringExtra("nik");
        tempatLahir = profileActivity.getIntent().getStringExtra("tempatLahir");
        tanggalLahir = profileActivity.getIntent().getStringExtra("tanggalLahir");
        kelamin = profileActivity.getIntent().getStringExtra("jenisKelamin");
        alamat = profileActivity.getIntent().getStringExtra("alamat");
        kelurahan = profileActivity.getIntent().getStringExtra("kelurahan");
        kecamatan = profileActivity.getIntent().getStringExtra("kecamatan");
        rtRw = profileActivity.getIntent().getStringExtra("rtRw");
        kotaKabupaten = profileActivity.getIntent().getStringExtra("kotaKabupaten");
        provinsi = profileActivity.getIntent().getStringExtra("provinsi");
        phone = profileActivity.getIntent().getStringExtra("phone");
        idPkh = profileActivity.getIntent().getStringExtra("idPkh");
        nama = profileActivity.getIntent().getStringExtra("nama");


        profileActivity.tvId.setText(idPkh);
        profileActivity.tvNama.setText(nama);
        profileActivity.etNik.setText(nik);
        profileActivity.etKotaKabupaten.setText(kotaKabupaten);
        profileActivity.etKecamatan.setText(kecamatan);
        profileActivity.etKelurahan.setText(kelurahan);
        profileActivity.etProvinsi.setText(provinsi);
        profileActivity.etAlamat.setText(alamat);
        profileActivity.etNoHp.setText(phone);
        profileActivity.etKelamin.setText(kelamin);

        profileActivity.etKelamin.setVisibility(View.GONE);
        profileActivity.btnSubmit.setText("UBAH DATA PKH");
        profileActivity.tvTitle.setText("PKH");

        profileActivity.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
