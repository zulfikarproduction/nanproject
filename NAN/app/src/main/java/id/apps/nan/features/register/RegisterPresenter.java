package id.apps.nan.features.register;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Map;

import id.apps.nan.features.otp.OtpActivity;
import id.apps.nan.util.ApiClient;
import id.apps.nan.util.Cfg;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter {
    Register register;


    public RegisterPresenter(Register register) {
        this.register = register;
    }

    public void getOtp(String nik, String nama, String hp, String id, String password, String email, String alamat, String kelurahan, String kecamatan, String kotaKabupaten, String provinsi, String kelamin, Boolean persetujuan){
        if (nik.equals("")||nama.equals("")||hp.equals("")||id.equals("")||password.equals("")||kelamin.equals("")||persetujuan==false){
            Toast.makeText(register, "data anda belum terisi", Toast.LENGTH_SHORT).show();
            return;
        }
        navigateToOTP(hp,email
//                "zulfikar@hike.id"
        );
    }

    public void navigateToOTP(String phone, String email){
        Intent intent = new Intent(register, OtpActivity.class);
        intent.putExtra("phone",phone);
        intent.putExtra("email", email);
        register.startActivityForResult(intent, 007);
    }

    public void doRegister(String nik, String nama, String hp, String id, String password, String email, String alamat, String kelurahan, String kecamatan, String kotaKabupaten, String provinsi,  String kelamin, String otp){
        SvRegister svRegister = ApiClient.serviceGenerator(Cfg.BASE_URL_IDENTITY).create(SvRegister.class);
        Call<Map<String,String>> call = svRegister.registrasi(nik, nama,hp, id, password,alamat, kelurahan, kecamatan, kotaKabupaten, provinsi , kelamin.substring(0,1),otp);
        call.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                if(!response.isSuccessful()||response.code()!=200){
                    Toast.makeText(register, "terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    return;
                }

                Map<String,String> res = response.body();
                if(!res.get("statusCode").equals("00")){
                    Toast.makeText(register, res.get("statusDesc"), Toast.LENGTH_LONG).show();
                    return;
                }

                Toast.makeText(register, "Registrasi berhasil", Toast.LENGTH_SHORT).show();
                register.getIntent().putExtra("id",id);
                register.getIntent().putExtra("password",password);
                register.setResult(00);
                register.finish();

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable throwable) {
                Toast.makeText(register, throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
