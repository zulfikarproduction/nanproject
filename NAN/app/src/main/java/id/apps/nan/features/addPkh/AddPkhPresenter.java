package id.apps.nan.features.addPkh;

import android.content.Intent;
import android.widget.Toast;

import java.util.Map;

import id.apps.nan.features.home.Home;
import id.apps.nan.util.ApiClient;
import id.apps.nan.util.Cfg;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPkhPresenter {
    AddPkhActivity addPkhActivity;

    private String nik,
            idPkh,
            namaPkh,
            tempatLahir,
            tanggalLahir,
            jenisKelamin,
            alamat,
            kelurahan,
            kecamatan,
            kotaKabupaten,
            provinsi,
            phone,
            rtRw,
            imageUrl;



    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getIdPkh() {
        return idPkh;
    }

    public void setIdPkh(String idPkh) {
        this.idPkh = idPkh;
    }

    public String getNamaPkh() {
        return namaPkh;
    }

    public void setNamaPkh(String namaPkh) {
        this.namaPkh = namaPkh;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKotaKabupaten() {
        return kotaKabupaten;
    }

    public void setKotaKabupaten(String kotaKabupaten) {
        this.kotaKabupaten = kotaKabupaten;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRtRw() {
        return rtRw;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public AddPkhPresenter(AddPkhActivity addPkhActivity) {
        this.addPkhActivity = addPkhActivity;
    }


    public void addPkhService(){
        String idPendamping = addPkhActivity.getSecPref().getString(Cfg.username,"");
        SvAddPkh service = ApiClient.serviceGenerator(Cfg.BASE_URL_IDENTITY).create(SvAddPkh.class);
        Call<Map<String,String>> call = service.addPkh(alamat,idPkh,jenisKelamin.substring(0,1),kecamatan,kelurahan,kotaKabupaten,namaPkh,nik,phone,provinsi,rtRw,tanggalLahir,tempatLahir, idPendamping);
        call.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                if (!response.isSuccessful()){
                    Toast.makeText(addPkhActivity, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    return;
                }

                Map<String,String> res = response.body();

                if(!res.get("statusCode").equals("00")){
                    Toast.makeText(addPkhActivity, res.get("statusDesc"), Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(addPkhActivity, "Berhasil menambahkan data Pkh", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(addPkhActivity, Home.class);
                addPkhActivity.startActivity(intent);

            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable t) {

            }
        });

    }
}
