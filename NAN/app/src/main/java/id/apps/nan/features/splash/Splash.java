package id.apps.nan.features.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.transition.Explode;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import java.net.InetAddress;

import id.apps.nan.R;
import id.apps.nan.features.login.Login;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setExitTransition(new Explode());
        setContentView(R.layout.activity_splash);



        setupWindowAnimations();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        checkConnection();

        new CountDownTimer(5000,1000){
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                doLogin();
            }
        }.start();

    }


    private void setupWindowAnimations() {
        Slide slide = new Slide();
        slide.setDuration(1000);
        getWindow().setExitTransition(slide);
    }

    private void checkConnection(){
        if (!isNetworkConnected() && !isInternetAvailable()){
//            Toast.makeText(getApplicationContext(),"Tidak ada Koneksi!",Toast.LENGTH_LONG).show();
            showDialogAlert();
        }

    }

    private void showDialogAlert() {
        Toast.makeText(this, "tidak terhubung jaringan", Toast.LENGTH_SHORT).show();
//        DialogAlertConnection dialogAlertConnection = new DialogAlertConnection();
//        dialogAlertConnection.show(getSupportFragmentManager(),"");
    }

    private boolean isInternetAvailable(){
        try{
            InetAddress ipAddr = InetAddress.getByName("www.google.com");
            return !ipAddr.equals("");
        }catch (Exception e){
            Log.e("ErrorConn",e.getMessage(),e.getCause());
            return false;
        }
    }

    private boolean isNetworkConnected(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        return isConnected;

    }

    private void doLogin(){
        Intent intent = new Intent(this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }


}
