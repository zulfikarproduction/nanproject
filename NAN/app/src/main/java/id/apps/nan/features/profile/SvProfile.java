package id.apps.nan.features.profile;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SvProfile {

    @GET("pendamping/profil")
    Call<Map<String,Object>> getProfilPendamping (@Query("id")String id);
}
