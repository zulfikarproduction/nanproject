package id.apps.nan.features.otp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Map;

import id.apps.nan.R;
import id.apps.nan.base.BaseActivity;
import id.apps.nan.features.register.SvRegister;
import id.apps.nan.util.ApiClient;
import id.apps.nan.util.Cfg;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends BaseActivity {

    EditText etOtp;
    Button btnNext;
    ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);


        initViews();
        getOtp();
    }

    private void initViews(){

        etOtp = findViewById(R.id.et_otp);
        btnNext = findViewById(R.id.btn_next);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("wakacawotp", etOtp.getText().toString().length()+"");
                if (etOtp.getText().toString().length()==6){
                    Log.d("wakacawotp", "true");
                    Intent intent = getIntent();
                    intent.putExtra("otp",etOtp.getText().toString());
                    setResult(00,intent);
                    finish();
                }
            }
        });
    }

    private void getOtp(){
        Bundle bundle = getIntent().getExtras();
        final String phone =
        bundle.getString("phone");
        final String email =
        bundle.getString("email");

        SvRegister svRegister = ApiClient.serviceGenerator(Cfg.BASE_URL_IDENTITY).create(SvRegister.class);
        Call<Map<String,String>> call = svRegister.getOtp(phone,email);
        call.enqueue(new Callback<Map<String, String>>() {
            @Override
            public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(OtpActivity.this, "terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String,String> res = response.body();
                if(!res.get("statusCode").equals("00")){
                    Toast.makeText(OtpActivity.this, "terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(OtpActivity.this, "OTP Telah dikirim", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Map<String, String>> call, Throwable e) {
                Toast.makeText(OtpActivity.this, "terjadi kesalahan : "+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }
}
