package id.apps.nan.features.addPkh;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface SvAddPkh {

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("pkh/add")
    @FormUrlEncoded
    Call<Map<String,String>> addPkh(@Field("alamat") String alamat,
                                    @Field("idPkh") String idPkh,
                                    @Field("jenisKelamin") String jenisKelamin,
                                    @Field("kecamatan") String kecamatan,
                                    @Field("kelurahan") String kelurahan,
                                    @Field("kotaKabupaten") String kotaKabupaten,
                                    @Field("namaPkh") String namaPkh,
                                    @Field("nik") String nik,
                                    @Field("phone") String phone,
                                    @Field("provinsi") String provinsi,
                                    @Field("rtRw") String rtRw,
                                    @Field("tanggalLahir") String tanggalLahir,
                                    @Field("tempatLahir") String tempatLahir,
                                    @Field("idPendamping") String idPendamping);
}
