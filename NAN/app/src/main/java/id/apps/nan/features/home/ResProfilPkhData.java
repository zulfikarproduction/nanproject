package id.apps.nan.features.home;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResProfilPkhData implements Serializable {

    private String idPkh;
    private String idPendamping;
    private String nik;
    private String namaPkh;
    private String tempatLahir;
    private String tanggalLahir;
    private String jenisKelamin;
    private String alamat;
    private String rtRw;
    private String kelurahan;
    private String kecamatan;
    private String kotaKabupaten;
    private String provinsi;
    private String phone;
    private String imageUrl;

    public String getIdPkh() {
        return idPkh;
    }

    public void setIdPkh(String idPkh) {
        this.idPkh = idPkh;
    }

    public String getIdPendamping() {
        return idPendamping;
    }

    public void setIdPendamping(String idPendamping) {
        this.idPendamping = idPendamping;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNamaPkh() {
        return namaPkh;
    }

    public void setNamaPkh(String namaPkh) {
        this.namaPkh = namaPkh;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getRtRw() {
        return rtRw;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKotaKabupaten() {
        return kotaKabupaten;
    }

    public void setKotaKabupaten(String kotaKabupaten) {
        this.kotaKabupaten = kotaKabupaten;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "ResProfilPkhData{" +
                "idPkh='" + idPkh + '\'' +
                ", idPendamping='" + idPendamping + '\'' +
                ", nik='" + nik + '\'' +
                ", namaPkh='" + namaPkh + '\'' +
                ", tempatLahir='" + tempatLahir + '\'' +
                ", tanggalLahir='" + tanggalLahir + '\'' +
                ", jenisKelamin='" + jenisKelamin + '\'' +
                ", alamat='" + alamat + '\'' +
                ", rtRw='" + rtRw + '\'' +
                ", kelurahan='" + kelurahan + '\'' +
                ", kecamatan='" + kecamatan + '\'' +
                ", kotaKabupaten='" + kotaKabupaten + '\'' +
                ", provinsi='" + provinsi + '\'' +
                ", phone='" + phone + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
