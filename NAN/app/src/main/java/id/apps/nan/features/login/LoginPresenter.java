package id.apps.nan.features.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import id.apps.nan.features.home.Home;
import id.apps.nan.util.ApiClient;
import id.apps.nan.util.Cfg;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {

    Login login;
    SharedPreferences secPrefs;
    String nik,namaPendamping,jenisKelamin, phone, alamat, kelurahan, kecamatan, kotaKabupaten, provinsi;

    public LoginPresenter(Login login) {
        this.login = login;
//        this.secPrefs = secPrefs;
    }

    public void doLogin (String userName, String password){
//        Map<String,String> map = new HashMap<>();
//        map.put("username", userName);
//        map.put("password", password);
        SvLogin svLogin = ApiClient.serviceGenerator(Cfg.BASE_URL_IDENTITY).create(SvLogin.class);
        Call<Map<String,Object>> call =
                svLogin.login(
                userName, password
//                "111",
//                        "password"
                );
        call.enqueue(new Callback<Map<String,Object>>() {
            @Override
            public void onResponse(Call<Map<String,Object>> call, Response<Map<String,Object>> response) {
                if (response.code()!=200){
                    Toast.makeText(login, "terjadi kesalahan", Toast.LENGTH_SHORT).show();
                    return;
                }
                Map<String,Object> res = response.body();
                String status = res.get("statusCode").toString();
                if(!status.equals("00")){
                    Toast.makeText(login, res.get("statusDesc").toString(), Toast.LENGTH_SHORT).show();
                    return;
                }
                Gson gson = new Gson();
                String json =gson.toJson(response.body().get("data"));
                try {
                    JSONObject obj = new JSONObject(json);
                    Log.d("wakacaw", "bisa " + obj.getString("jenisKelamin"));
                    nik = obj.getString("nik");
                    namaPendamping = obj.getString("namaPendamping");
                    jenisKelamin = obj.getString("jenisKelamin");
                    phone = obj.getString("phone");
                    alamat = obj.getString("alamat");
                    kelurahan = obj.getString("kelurahan");
                    kecamatan = obj.getString("kecamatan");
                    kotaKabupaten = obj.getString("kotaKabupaten");
                    provinsi = obj.getString("provinsi");

                    Log.d("wakacaw", "bisa " + nik);

                } catch (Throwable t) {
                    Log.e("wakacaw", "Could not parse malformed JSON: \"" + json + "\"");
                }
//                Log.d("wakacaw",json);
                login.getSecPref().edit().putString(Cfg.nik,nik).apply();
                login.getSecPref().edit().putString(Cfg.namaPendamping,namaPendamping).apply();
                login.getSecPref().edit().putString(Cfg.jenisKelamin,jenisKelamin).apply();
                login.getSecPref().edit().putString(Cfg.phone,phone).apply();
                login.getSecPref().edit().putString(Cfg.alamat,alamat).apply();
                login.getSecPref().edit().putString(Cfg.kelurahan,kelurahan).apply();
                login.getSecPref().edit().putString(Cfg.kecamatan,kecamatan).apply();
                login.getSecPref().edit().putString(Cfg.kotaKabupaten,kotaKabupaten).apply();
                login.getSecPref().edit().putString(Cfg.provinsi,provinsi).apply();

                login.getSecPref().edit().putString(Cfg.username,userName).apply();
                login.getSecPref().edit().putString(Cfg.password,password).apply();
                navigateToHome();


            }

            @Override
            public void onFailure(Call<Map<String,Object>> call, Throwable throwable) {

            }
        });
    }

    public void navigateToHome(){
        Toast.makeText(login, "Login Berhasil", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(login, Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        login.startActivity(intent);
    }
}
