package id.apps.nan.features.addPkh.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.apps.nan.R;
import id.apps.nan.base.BaseFragment;
import id.apps.nan.features.addPkh.AddPkhActivity;


public class AddFragment1 extends BaseFragment {

    @BindView(R.id.et_id_pkh)
    TextView etIdPkh;
    @BindView(R.id.et_nik)
    TextView etNik;
    @BindView(R.id.btn_next)
    Button btnNext;



    private OnFragmentInteractionListener mListener;

    public AddFragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_fragment1, container, false);
        ButterKnife.bind(this,view);


        return view;
    }

    @OnClick(R.id.btn_next)
    public void next(){
        String idPkh = etIdPkh.getText().toString();
        String nik = etNik.getText().toString();

        if (!idPkh.equals("")&&!nik.equals(""))
        {
            ((AddPkhActivity)getActivity()).addPkhPresenter.setIdPkh(idPkh);
            ((AddPkhActivity)getActivity()).addPkhPresenter.setNik(nik);

            Log.d("wakacawa", ((AddPkhActivity)getActivity()).addPkhPresenter.getIdPkh());

//            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragmentLayout, new AddFragment2()).commit();
            ((AddPkhActivity)getActivity()).navigateBetweenFragment("addFragment2");
            return;
        }

        Toast.makeText(getActivity(), "Isi data!", Toast.LENGTH_SHORT).show();


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        etIdPkh.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getIdPkh());
        etNik.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getNik());

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
