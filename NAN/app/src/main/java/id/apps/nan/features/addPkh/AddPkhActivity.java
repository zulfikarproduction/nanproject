package id.apps.nan.features.addPkh;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.apps.nan.R;
import id.apps.nan.base.BaseActivity;
import id.apps.nan.features.addPkh.fragment.AddFragment1;
import id.apps.nan.features.addPkh.fragment.AddFragment2;
import id.apps.nan.features.addPkh.fragment.VerifikasiFragment;

public class AddPkhActivity extends BaseActivity {

    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.btn_back)
    public ImageView btnBack;
    @BindView(R.id.fragmentLayout) LinearLayout fragmentLayout;
    FragmentManager fragmentManager;
    public AddPkhPresenter addPkhPresenter;
    AddFragment1 addFragment1;
    AddFragment2 addFragment2;
    VerifikasiFragment verifikasiFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_pkh);
        ButterKnife.bind(this);

        firstInit();
    }

    private void firstInit(){
        addFragment1 = new AddFragment1();
        addFragment2 = new AddFragment2();
        verifikasiFragment = new VerifikasiFragment();
        addPkhPresenter = new AddPkhPresenter(this);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentLayout, new AddFragment1()).commit();

    }

    @OnClick(R.id.btn_back)
    public void back() {
        Fragment f = fragmentManager.getFragments().size() > 0 ? fragmentManager.getFragments().get(0) : null;
        Log.d("wakacawfragment", fragmentManager.getFragments().toString());

        if (f != null && f instanceof AddFragment1) {
//            fragmentManager.popBackStack();
//            tabSearch.setVisibility(View.VISIBLE);
            onBackPressed();
            return;
        }

        if (f != null && f instanceof AddFragment2)
        {
            ((AddFragment2) f).onBackPressed();
            return;
        }

        if (f != null && f instanceof VerifikasiFragment)
        {
            ((VerifikasiFragment) f).onBackPressed();
            return;
        }


    }

    public void navigateBetweenFragment(String fragment){
        if(fragment.equals("addFragment2"))
        {
            fragmentManager.beginTransaction().replace(R.id.fragmentLayout, addFragment2).commit();
            return;
        }
        if(fragment.equals("verifikasiFragment"))
        {
            fragmentManager.beginTransaction().replace(R.id.fragmentLayout, verifikasiFragment).commit();
            return;
        }

        if(fragment.equals("addFragment1"))
        {
            fragmentManager.beginTransaction().replace(R.id.fragmentLayout, addFragment1).commit();
            return;
        }

    }


}
