package id.apps.nan.features.home;

import java.util.Map;

import id.apps.nan.global.ResponseData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SvHome {
    @GET("pkh/get-by-pendamping")
    Call <ResProfilPkh> getPkh(@Query("id")String id);
}


