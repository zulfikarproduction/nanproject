package id.apps.nan.features.addPkh.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.apps.nan.R;
import id.apps.nan.base.BaseFragment;
import id.apps.nan.features.addPkh.AddPkhActivity;

public class VerifikasiFragment extends BaseFragment {
    private OnFragmentInteractionListener mListener;

    @BindView(R.id.tv_nama)
    TextView tvNama;
    @BindView(R.id.tv_nik) TextView tvNik;
    @BindView(R.id.tv_id) TextView tvId;
    @BindView(R.id.tv_alamat)TextView tvAlamat;
    @BindView(R.id.tv_kelurahan) TextView tvKelurahan;
    @BindView(R.id.tv_kecamatan) TextView tvKecamatan;
    @BindView(R.id.tv_kotakabupaten) TextView tvKotaKabupaten;
    @BindView(R.id.tv_provinsi) TextView tvProvinsi;
    @BindView(R.id.tv_phone) TextView tvHp;
    @BindView(R.id.tv_kelamin)
    TextView tvKelamin;
    @BindView(R.id.tv_tempat_lahir) TextView tvTempatLahir;
    @BindView(R.id.tv_tanggal_lahir) TextView tvTanggalLahir;
    @BindView(R.id.tv_rtrw) TextView tvRtRw;
    @BindView(R.id.btn_submit)
    Button btnSubmit;



    public VerifikasiFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_verifikasi, container, false);
        ButterKnife.bind(this,view);

        initView();
        return view;
    }

    private void initView(){
        String idPkh,
                nik,
                namaPkh,
                tempatLahir,
                tanggalLahir,
                jenisKelamin,
                alamat,
                kelurahan,
                kecamatan,
                kotaKabupaten,
                provinsi,
                phone,
                rtRw,
                imageUrl;

        idPkh = ((AddPkhActivity)getActivity()).addPkhPresenter.getIdPkh();
        nik = ((AddPkhActivity)getActivity()).addPkhPresenter.getNik();
        namaPkh = ((AddPkhActivity)getActivity()).addPkhPresenter.getNamaPkh();
        tempatLahir = ((AddPkhActivity)getActivity()).addPkhPresenter.getTempatLahir();
        tanggalLahir = ((AddPkhActivity)getActivity()).addPkhPresenter.getTanggalLahir();
        jenisKelamin = ((AddPkhActivity)getActivity()).addPkhPresenter.getJenisKelamin();
        alamat = ((AddPkhActivity)getActivity()).addPkhPresenter.getAlamat();
        kelurahan = ((AddPkhActivity)getActivity()).addPkhPresenter.getKelurahan();
        kecamatan = ((AddPkhActivity)getActivity()).addPkhPresenter.getKecamatan();
        kotaKabupaten = ((AddPkhActivity)getActivity()).addPkhPresenter.getKotaKabupaten();
        provinsi = ((AddPkhActivity)getActivity()).addPkhPresenter.getProvinsi();
        phone = ((AddPkhActivity)getActivity()).addPkhPresenter.getPhone();
        rtRw = ((AddPkhActivity)getActivity()).addPkhPresenter.getRtRw();


        tvId.setText(idPkh);
        tvNama.setText(namaPkh);
        tvNik.setText(nik);
        tvTempatLahir.setText(tempatLahir);
        tvTanggalLahir.setText(tanggalLahir);
        tvKelamin.setText(jenisKelamin);
        tvAlamat.setText(alamat);
        tvKelurahan.setText(kelurahan);
        tvKecamatan.setText(kecamatan);
        tvKotaKabupaten.setText(kotaKabupaten);
        tvProvinsi.setText(provinsi);
        tvHp.setText(phone);
        tvRtRw.setText(rtRw);


        Log.d("wakacawadd", ((AddPkhActivity)getActivity()).addPkhPresenter.getIdPkh());
    }

    @OnClick(R.id.btn_submit)
    public void submit(){
        ((AddPkhActivity)getActivity()).addPkhPresenter.addPkhService();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }


    public void onBackPressed(){
        ((AddPkhActivity)getActivity()).navigateBetweenFragment("addFragment2");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
