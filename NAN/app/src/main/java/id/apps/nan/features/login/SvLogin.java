package id.apps.nan.features.login;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface SvLogin {

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("auth/login")
    @FormUrlEncoded
    Call<Map<String, Object>> login (@Field("username") String username,
                                      @Field("password") String password);


}
