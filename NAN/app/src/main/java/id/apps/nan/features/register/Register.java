package id.apps.nan.features.register;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.apps.nan.R;
import id.apps.nan.base.BaseActivity;
import id.apps.nan.util.ArrayAdapterFlex;

public class Register extends BaseActivity {

    EditText etNik, etNama, etHp, etId, etPassword, etEmail, etAlamat, etKelurahan, etKecamatan, etKotaKabupaten, etprovinsi;
    Spinner spinnerKelamin;
    String nik,nama,hp,id,password, kelamin, email, alamat, kelurahan,kecamatan, kotaKabupaten, provinsi;
    Boolean setuju;
    ImageView btnBack;
    List<String> kelaminData = Arrays.asList("JENIS KELAMIN", "Laki - Laki", "Perempuan");
    CheckBox cbPersetujuan;
    Button btnRegister;
    RegisterPresenter registerPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();
    }

    private void initViews() {
        btnBack = findViewById(R.id.btn_back);
        registerPresenter = new RegisterPresenter(this);
        etNik = findViewById(R.id.et_nik);
        etNama = findViewById(R.id.et_nama);
        etHp = findViewById(R.id.et_hp);
        etId = findViewById(R.id.et_id_pendamping);
        etPassword = findViewById(R.id.et_password);
        cbPersetujuan = findViewById(R.id.checkbox);
        spinnerKelamin = findViewById(R.id.spinner_kelamin);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.kelamin_list, android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapterFlex adapterSpinner = new ArrayAdapterFlex(this, android.R.layout.simple_spinner_dropdown_item, kelaminData);
        spinnerKelamin.setAdapter(adapterSpinner);

        etEmail = findViewById(R.id.et_email);
        etAlamat = findViewById(R.id.et_alamat);
        etKelurahan = findViewById(R.id.et_kelurahan);
        etKecamatan = findViewById(R.id.et_kecamatan);
        etKotaKabupaten = findViewById(R.id.et_kotakabupaten);
        etprovinsi = findViewById(R.id.et_provinsi);



        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nik= etNik.getText().toString();
                        nama=etNama.getText().toString();
                        hp=etHp.getText().toString();
                        id=etId.getText().toString();
                        password=etPassword.getText().toString();
                        kelamin=spinnerKelamin.getSelectedItem().toString();
                        email = etEmail.getText().toString();
                        alamat = etAlamat.getText().toString();
                        kelurahan = etKelurahan.getText().toString();
                        kecamatan = etKecamatan.getText().toString();
                        kotaKabupaten = etKotaKabupaten.getText().toString();
                        provinsi = etprovinsi.getText().toString();


                        setuju=cbPersetujuan.isChecked();

                registerPresenter.getOtp(
                        nik,nama,hp,id,password,email,alamat,kelurahan,kecamatan,kotaKabupaten,provinsi,kelamin,setuju);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==007&&resultCode==00){
            registerPresenter.doRegister(nik,nama,hp,id,password,email,alamat,kelurahan,kecamatan,kotaKabupaten,provinsi,kelamin,data.getExtras().getString("otp"));
        }
    }
}
