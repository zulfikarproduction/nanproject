package id.apps.nan.global;

import java.io.Serializable;

public class ResponseData<Any> implements Serializable {
    private String statusCode;
    private String statusDesc;
    private Any data;

    public String getStatus() {
        return statusCode;
    }

    public void setStatus(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return statusDesc;
    }

    public void setMessage(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public Any getData() {
        return data;
    }

    public void setData(Any data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseBody{" +
                "status='" + statusCode + '\'' +
                ", message='" + statusDesc + '\'' +
                ", data=" + data +
                '}';
    }
}



