package id.apps.nan.features.register;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface SvRegister {

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("register/pendamping")
    @FormUrlEncoded
    Call<Map<String, String>> registrasi (@Field("nik") String nik,
                                          @Field("namaPendamping") String nama,
                                          @Field("phone") String phone,
                                          @Field("idPendamping") String id,
                                          @Field("password") String password,
                                          @Field("alamat") String alamat,
                                          @Field("kelurahan") String kelurahan,
                                          @Field("kecamatan") String kecamatan,
                                          @Field("kotaKabupaten") String kotaKabupaten,
                                          @Field("provinsi") String provinsi,
                                          @Field("jenisKelamin") String kelamin,
                                          @Field("otp") String otp
                                          );

    @POST("register/request-otp")
    @FormUrlEncoded
    Call<Map<String,String>> getOtp(@Field("phone") String phone,
                                    @Field("email") String email);

}
