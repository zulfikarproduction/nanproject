package id.apps.nan.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TextFormatter  {

    public TextFormatter() {
    }


    public String formatRtRw(String string){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance();
        if(string.length()==2){
            formatter.applyPattern("#,#");
        }
        if (string.length()==4){
            formatter.applyPattern("##,##");
        }
        if (string.length()==6){
            formatter.applyPattern("###,###");
        }
        long l = Long.parseLong(string);
        String formattedString = formatter.format(l);
        return (formattedString.replace(",","/"));

    }
}
