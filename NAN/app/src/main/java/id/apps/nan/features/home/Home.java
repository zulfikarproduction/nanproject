package id.apps.nan.features.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.apps.nan.R;
import id.apps.nan.base.BaseActivity;
import id.apps.nan.features.addPkh.AddPkhActivity;
import id.apps.nan.features.profile.ProfileActivity;
import id.apps.nan.features.home.adapter.RvCustomAdapter;
import id.apps.nan.global.ResponseData;
import id.apps.nan.util.ApiClient;
import id.apps.nan.util.Cfg;
import id.apps.nan.util.ListenerRecyclerItemClick;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Consumer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends BaseActivity {


    private long mBackPressed;
    private static final int TIME_INTERVAL = 2000;

    @BindView(R.id.user) TextView tvUser;
    @BindView(R.id.btn_menu) ImageView btnMenu;
    @BindView(R.id.rv_pkh) RecyclerView rvPkh;
    @BindView(R.id.btn_tambah) Button btnTambah;
    private String id;

    private LinearLayoutManager linearLayoutManager;
    private RvCustomAdapter rvCustomAdapter;
    HomePresenter homePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        initView();
    }

    private void initView(){
        homePresenter = new HomePresenter(this);
        tvUser.setText("Halo, "+getSecPref().getString(Cfg.namaPendamping,""));
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToProfile();
            }
        });

        populateRv();

    }

    @OnClick(R.id.btn_tambah)
    void tambahProfile(){
        Intent intent = new Intent(this, AddPkhActivity.class);
//        intent.putExtra("profil", "cpkh");
        startActivity(intent);
    }

    private void populateRv(){
        id = getSecPref().getString(Cfg.username,"");
        linearLayoutManager = new LinearLayoutManager(this);
//        rvCustomAdapter = new RvCustomAdapter();

//        homePresenter.getPkhByid(id);
        callService(id);
//        callService("111");
    }

    private void callService(String id){
        ResProfilPkh resmm;
        resmm = new ResProfilPkh();

        SvHome service = ApiClient.serviceGenerator(Cfg.BASE_URL_IDENTITY).create(SvHome.class);
        Call<ResProfilPkh> call = service.getPkh(id);
        call.enqueue(new Callback<ResProfilPkh>() {
            @Override
            public void onResponse(Call<ResProfilPkh> call, Response<ResProfilPkh> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(Home.this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    return;
                }

                ResProfilPkh res = response.body();
                if(!res.getStatus().equals("00")){
                    Toast.makeText(Home.this, "Terjadi Kesalahan", Toast.LENGTH_SHORT).show();
                    return;
                }

                List<ResProfilPkhData> dataPkh = new ArrayList<>();
                dataPkh=response.body().getData();
                resmm.setData(dataPkh);
                rvCustomAdapter = new RvCustomAdapter(resmm,getApplicationContext());
                rvPkh.setAdapter(rvCustomAdapter);
                rvPkh.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                Log.d("wakacaw",dataPkh.toString());

                rvPkh.addOnItemTouchListener(new ListenerRecyclerItemClick(getApplicationContext(), new ListenerRecyclerItemClick.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        RvCustomAdapter rv =(RvCustomAdapter)rvPkh.getAdapter();
                        int rvMax = rv.mm.getData().size();
                        Log.d("wakacawrv",resmm.getData().get(position).getNamaPkh());

                        Intent intent = new Intent(Home.this, ProfileActivity.class);
                        intent.putExtra("profil","ppkh");
                        intent.putExtra("nama",resmm.getData().get(position).getNamaPkh());
                        intent.putExtra("idPkh",resmm.getData().get(position).getIdPkh());
                        intent.putExtra("nik",resmm.getData().get(position).getNik());
                        intent.putExtra("tempatLahir",resmm.getData().get(position).getTempatLahir());
                        intent.putExtra("tanggalLahir",resmm.getData().get(position).getTanggalLahir());
                        intent.putExtra("jenisKelamin",resmm.getData().get(position).getJenisKelamin());
                        intent.putExtra("alamat",resmm.getData().get(position).getAlamat());
                        intent.putExtra("kelurahan",resmm.getData().get(position).getKelurahan());
                        intent.putExtra("kecamatan",resmm.getData().get(position).getKecamatan());
                        intent.putExtra("rtRw",resmm.getData().get(position).getRtRw());
                        intent.putExtra("kotaKabupaten",resmm.getData().get(position).getKotaKabupaten());
                        intent.putExtra("provinsi",resmm.getData().get(position).getProvinsi());
                        intent.putExtra("phone",resmm.getData().get(position).getPhone());
                        startActivity(intent);

                    }
                }));
            }

            @Override
            public void onFailure(Call<ResProfilPkh> call, Throwable t) {

            }
        });

    }

    private void navigateToProfile(){
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("profil", "pendamping");
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis())
        {
            minimizeApp();
            return;
        }
        else { Toast.makeText(getBaseContext(), "tekan dua kali untuk keluar aplikasi", Toast.LENGTH_SHORT).show(); }

        mBackPressed = System.currentTimeMillis();
    }

    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
}
