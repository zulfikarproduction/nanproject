package id.apps.nan.util;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ArrayAdapterFlex extends ArrayAdapter<String> {
    public ArrayAdapterFlex(Context ctx, int resourceId, List<String> data) {
        super(ctx, resourceId, data);
    }

    @Override
    public boolean isEnabled(int position){
        if(position == 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = super.getView(position,convertView,parent);
        TextView tv = (TextView) v;

        if(position == 0){
            tv.setTextColor(Color.GRAY);
        }
        else {
            tv.setTextColor(Color.BLACK);
        }

        return v;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        TextView tv = (TextView) view;

        if(position == 0) {
            tv.setTextColor(Color.GRAY);
        } else {
            tv.setTextColor(Color.BLACK);
        }

        return view;
    }

    @Override
    public String getItem(int position) {
        String item = super.getItem(position);

        if (item != null && item.contains("|")) {
            return item.split("\\|")[0];
        }

        return item;
    }

    public Object getItemIdObj(int position) {
        String item = super.getItem(position);

        if (item.contains("|")) {
            return item.split("\\|")[1];
        } else {
            return null;
        }
    }
}
