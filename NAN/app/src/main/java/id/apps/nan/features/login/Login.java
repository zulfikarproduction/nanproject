package id.apps.nan.features.login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.apps.nan.R;
import id.apps.nan.base.BaseActivity;
import id.apps.nan.features.register.Register;

public class Login extends BaseActivity {

//    @BindView(R.id.btn_register)
    Button btnRegister, btnLogin;
    EditText etUsername, etPassword;
    LoginPresenter loginPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        ButterKnife.bind(this);

        initViews();

    }

    private void initViews(){
        loginPresenter = new LoginPresenter(this);
        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRegister();
            }
        });
        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin(etUsername.getText().toString(), etPassword.getText().toString());
            }
        });
        etUsername = findViewById(R.id.et_id);
        etPassword = findViewById(R.id.et_password);

    }

//    @OnClick(R.id.btn_register)
    public void doRegister(){
        Intent intent = new Intent(this, Register.class);
        startActivityForResult(intent, 111);
    }

    public void doLogin(String userName, String password){
        loginPresenter.doLogin(userName, password);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==11&&resultCode==00){
            doLogin(
            data.getExtras().getString("id"),
            data.getExtras().getString("password")
            );
        }
    }

    @Override
    public void onBackPressed() {
        minimizeApp();
    }

    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
}
