package id.apps.nan.features.addPkh.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.apps.nan.R;
import id.apps.nan.base.BaseFragment;
import id.apps.nan.features.addPkh.AddPkhActivity;
import id.apps.nan.util.ArrayAdapterFlex;
import id.apps.nan.util.TextFormatter;

public class AddFragment2 extends BaseFragment {

    private OnFragmentInteractionListener mListener;


    List<String> kelaminData = Arrays.asList("JENIS KELAMIN", "Laki - Laki", "Perempuan");

    @BindView(R.id.et_nama)
    EditText etNama;
    @BindView(R.id.et_alamat)EditText etalamat;
    @BindView(R.id.et_kelurahan) EditText etKelurahan;
    @BindView(R.id.et_kecamatan) EditText etKecamatan;
    @BindView(R.id.et_kotakabupaten) EditText etKotaKabupaten;
    @BindView(R.id.et_provinsi) EditText etProvinsi;
    @BindView(R.id.et_nomor_handphone) EditText etHp;
    @BindView(R.id.spinner_kelamin) Spinner spinnerKelamin;
    @BindView(R.id.et_tempat_lahir) EditText etTempatLahir;
    @BindView(R.id.et_tanggal_lahir) EditText etTanggalLahir;
    @BindView(R.id.et_rtrw) EditText etRtRw;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    TextFormatter textFormatter = new TextFormatter();


    public AddFragment2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_fragment2, container, false);
        ButterKnife.bind(this,view);

        firstInit();
        return view;
    }

    void firstInit(){
        ArrayAdapterFlex adapterSpinner = new ArrayAdapterFlex(getContext(), android.R.layout.simple_spinner_dropdown_item, kelaminData);
        spinnerKelamin.setAdapter(adapterSpinner);


        etRtRw.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                int stringLength= s.toString().replace("/","").length();
                if (stringLength==2||stringLength==4||stringLength==6){
                }
                else {
                    Toast.makeText(getContext(), "masukan dengan format 0102 untuk RT 01, Rw 02", Toast.LENGTH_SHORT).show();
                }

            }
        });
        Log.d("wakacaw", ((AddPkhActivity)getActivity()).addPkhPresenter.getIdPkh());
    }


    @OnClick(R.id.btn_submit)
    public void submit(){
        String nik,
                namaPkh,
                tempatLahir,
                tanggalLahir,
                jenisKelamin,
                alamat,
                kelurahan,
                kecamatan,
                kotaKabupaten,
                provinsi,
                phone,
                rtRw,
                imageUrl;

        namaPkh = etNama.getText().toString();
        tempatLahir = etTempatLahir.getText().toString();
        tanggalLahir = etTanggalLahir.getText().toString();
        jenisKelamin = spinnerKelamin.getSelectedItem().toString();
        alamat = etalamat.getText().toString();
        kelurahan = etKelurahan.getText().toString();
        kecamatan = etKecamatan.getText().toString();
        kotaKabupaten = etKotaKabupaten.getText().toString();
        provinsi = etProvinsi.getText().toString();
        phone = etHp.getText().toString();
        rtRw = textFormatter.formatRtRw(
                etRtRw.getText().toString());
        if (
                namaPkh.equals("")&&
                tempatLahir.equals("")&&
                        tanggalLahir.equals("")&&
                        jenisKelamin.equals("")&&
                        alamat.equals("")&&
                        kelurahan.equals("")&&
                        kecamatan.equals("")&&
                        kotaKabupaten.equals("")&&
                        provinsi.equals("")&&
                        phone.equals("")){
            Toast.makeText(getContext(), "Lengkapi data Anda", Toast.LENGTH_SHORT).show();
            return;
        }

        ((AddPkhActivity)getActivity()).addPkhPresenter.setNamaPkh(namaPkh);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setTempatLahir(tempatLahir);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setTanggalLahir(tanggalLahir);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setJenisKelamin(jenisKelamin);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setAlamat(alamat);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setKelurahan(kelurahan);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setKecamatan(kecamatan);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setKotaKabupaten(kotaKabupaten);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setProvinsi(provinsi);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setPhone(phone);
        ((AddPkhActivity)getActivity()).addPkhPresenter.setRtRw(rtRw);

        ((AddPkhActivity)getActivity()).navigateBetweenFragment("verifikasiFragment");

    }

    @Override
    public void onResume() {
        super.onResume();
        etNama.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getNamaPkh());
        etTempatLahir.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getTempatLahir());
        etTanggalLahir.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getTanggalLahir());
//        spinnerKelamin.setSelection(((AddPkhActivity)getActivity()).addPkhPresenter.getJenisKelamin().equals("Laki - Laki")? 1:2);
        etalamat.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getAlamat());
        etKelurahan.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getKelurahan());
        etKecamatan.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getKecamatan());
        etKotaKabupaten.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getKotaKabupaten());
        etProvinsi.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getProvinsi());
        etHp.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getPhone());
        etRtRw.setText(((AddPkhActivity)getActivity()).addPkhPresenter.getRtRw());
    }

    public void onBackPressed(){
        ((AddPkhActivity)getActivity()).navigateBetweenFragment("addFragment1");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
