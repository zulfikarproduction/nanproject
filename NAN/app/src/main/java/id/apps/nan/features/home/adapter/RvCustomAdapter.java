package id.apps.nan.features.home.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.apps.nan.R;
import id.apps.nan.features.home.ResProfilPkh;
import id.apps.nan.features.home.ResProfilPkhData;

public class RvCustomAdapter extends RecyclerView.Adapter<RvCustomAdapter.MyViewHolder> {

    public ResProfilPkh mm;
    private Context context;

    public RvCustomAdapter(ResProfilPkh mm, Context context) {
        this.mm = mm;
        this.context = context;
    }


    private final List<String> stringValues = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_nama_pkh)
        TextView namaPkh;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this,itemView);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.i_ppkh, parent, false);
//        MyViewHolder myViewHolder = new MyViewHolder(v);


        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.namaPkh.setText(mm.getData().get(position).getNamaPkh());
        Log.d("wakacawadapt", position+"");
        Log.d("wakacawadapt",mm.getData().get(position).getNamaPkh());
    }

    @Override
    public int getItemCount() {
        return mm.getData().size();
    }

    public void addStringToList (String value){
        stringValues.add(value);
        notifyItemInserted(stringValues.size()-1);
    }

    public void addData (List<ResProfilPkhData> modeldata){
        mm.getData().addAll(modeldata);
//        notifyDataSetChanged();
        notifyItemInserted(mm.getData().size()-1);
        mm.getData().size();
        Log.d("wakacawadapt",mm.getData().get(0).getNamaPkh());

    }



}

