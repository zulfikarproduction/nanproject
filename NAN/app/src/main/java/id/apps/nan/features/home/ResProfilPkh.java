package id.apps.nan.features.home;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResProfilPkh implements Serializable {
    @SerializedName("statusCode")
    private String status;

    @SerializedName("statusDesc")
    private String message;

    @SerializedName("data")
    private List<ResProfilPkhData> data;

    @Override
    public String toString() {
        return "ResProfilPkh{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ResProfilPkhData> getData() {
        return data;
    }

    public void setData(List<ResProfilPkhData> data) {
        this.data = data;
    }
}
